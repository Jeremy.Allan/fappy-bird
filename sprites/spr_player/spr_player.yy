{
    "id": "03ab947f-62f2-45c4-8e15-fa1fe21cc5bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "014e21d9-7ee5-4c42-8075-33823b34154f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03ab947f-62f2-45c4-8e15-fa1fe21cc5bb",
            "compositeImage": {
                "id": "0b49d4ae-97d2-4fac-8661-db8307c8e991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "014e21d9-7ee5-4c42-8075-33823b34154f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e14a97-08aa-4819-9ea4-13dddee555fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "014e21d9-7ee5-4c42-8075-33823b34154f",
                    "LayerId": "b61f0124-91f5-412e-8018-6752dfb86a14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b61f0124-91f5-412e-8018-6752dfb86a14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03ab947f-62f2-45c4-8e15-fa1fe21cc5bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}