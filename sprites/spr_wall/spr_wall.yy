{
    "id": "61387c79-d9d5-4b51-baf0-053b3bf3882d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39209c66-bccd-4349-8a8d-1233bf9c19fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61387c79-d9d5-4b51-baf0-053b3bf3882d",
            "compositeImage": {
                "id": "df180080-fc40-4514-bf66-db12ac7aacfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39209c66-bccd-4349-8a8d-1233bf9c19fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "514fe9ab-417f-4e9c-8510-00bad560f3d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39209c66-bccd-4349-8a8d-1233bf9c19fe",
                    "LayerId": "7ca0d250-ee1f-4ca7-849c-41caf7d4d704"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7ca0d250-ee1f-4ca7-849c-41caf7d4d704",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61387c79-d9d5-4b51-baf0-053b3bf3882d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}