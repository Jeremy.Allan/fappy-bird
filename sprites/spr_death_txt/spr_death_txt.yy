{
    "id": "384f63d1-7ff0-4557-9b13-122f25748c18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_death_txt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 576,
    "bbox_left": 96,
    "bbox_right": 909,
    "bbox_top": 180,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9c3f1bb-5686-4ab0-9fc2-26c829d48006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "dcda1664-02ed-4337-b7c7-cbb0332f9602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9c3f1bb-5686-4ab0-9fc2-26c829d48006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b3f1b4-3d3d-4f87-80d7-d55f229faf26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9c3f1bb-5686-4ab0-9fc2-26c829d48006",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "9a6c3170-6e16-493b-8fe5-9783299ead7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "5045885f-9653-4a3b-8169-4db2e5979df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a6c3170-6e16-493b-8fe5-9783299ead7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d881ab-9500-4ebd-bdc2-b624db1a20f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a6c3170-6e16-493b-8fe5-9783299ead7e",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "aa2d389c-2d17-4e38-8666-011e49a911e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "0a804e9d-5d62-47f9-ba0e-adfb971ac0a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2d389c-2d17-4e38-8666-011e49a911e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef5c3fa5-8e69-4f0e-be09-873b141b7358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2d389c-2d17-4e38-8666-011e49a911e6",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "e80f597a-e3f6-4b43-9dc0-bae666f311fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "4fcc61f5-fd52-4938-aa28-0fd666de8a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80f597a-e3f6-4b43-9dc0-bae666f311fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "532523bc-174f-417b-ba68-a31f540003cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80f597a-e3f6-4b43-9dc0-bae666f311fb",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "dcdc10fa-b3c8-4b62-b8d7-c4699b805e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "33662a37-396d-48e8-9903-1f2ab3c3d4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcdc10fa-b3c8-4b62-b8d7-c4699b805e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c074414-98c7-4b26-8366-ca0ffaa32883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcdc10fa-b3c8-4b62-b8d7-c4699b805e77",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "a120d7ce-8960-43fb-bae4-44568a5dd724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "3c37d81f-dc3c-4605-b3e1-572499cc2820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a120d7ce-8960-43fb-bae4-44568a5dd724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ba72c3-fe4a-4b4b-ba99-6c2c230c940d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a120d7ce-8960-43fb-bae4-44568a5dd724",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "1daa0010-1d68-497f-885b-4ecbe8f9654b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "4f9af76a-7c01-4f44-9c76-448088222cb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1daa0010-1d68-497f-885b-4ecbe8f9654b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5269cfb8-fdf6-4670-a628-ac76383d039c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1daa0010-1d68-497f-885b-4ecbe8f9654b",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "3b9d790f-639c-4542-bc8e-47de69ed20ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "03334278-a893-4b47-b0b3-f11555387bcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b9d790f-639c-4542-bc8e-47de69ed20ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74577658-89c2-47a5-b231-ddb570284835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b9d790f-639c-4542-bc8e-47de69ed20ad",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        },
        {
            "id": "56577cf1-ba16-4386-bb34-9efab020ec51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "compositeImage": {
                "id": "2bc8b041-685c-4908-ba11-2b156c6af340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56577cf1-ba16-4386-bb34-9efab020ec51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f12b830d-1e1e-4383-8b4f-17387108e3f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56577cf1-ba16-4386-bb34-9efab020ec51",
                    "LayerId": "00c25249-d9c8-4fab-8117-49bc399a2cb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "00c25249-d9c8-4fab-8117-49bc399a2cb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}