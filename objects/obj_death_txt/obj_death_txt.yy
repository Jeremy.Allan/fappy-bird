{
    "id": "6731c7bc-47ea-4089-bf3e-40ac84c8beab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_txt",
    "eventList": [
        {
            "id": "3563e289-65cc-416a-b959-affc2abc2ce3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "6731c7bc-47ea-4089-bf3e-40ac84c8beab"
        },
        {
            "id": "8c90acb1-c60f-4773-ae7a-0d17153fbe3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6731c7bc-47ea-4089-bf3e-40ac84c8beab"
        },
        {
            "id": "eaf68524-5cb1-4a70-8ee8-e176b06fb6ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6731c7bc-47ea-4089-bf3e-40ac84c8beab"
        },
        {
            "id": "aff880e1-50c4-43fd-b391-027052c34989",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "6731c7bc-47ea-4089-bf3e-40ac84c8beab"
        },
        {
            "id": "656512c2-e69f-40e2-abb2-93cb35c8ff70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 13,
            "m_owner": "6731c7bc-47ea-4089-bf3e-40ac84c8beab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "384f63d1-7ff0-4557-9b13-122f25748c18",
    "visible": true
}