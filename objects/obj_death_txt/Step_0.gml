/// @description keep blinking time
// You can write your code in this editor

if (!title_state)
{
	if (image_index >= 7)
	{
		image_speed = 0;
		title_state = 1;
	}
}
else if (title_state)
{
	image_speed = 0;
	
	if (blink_counter >= 0)
	{
		blink_counter--;
	}
	else
	{
		if (image_index == 7) image_index++;
		else if (image_index == 8) image_index--;
		
		blink_counter = blink_counter_max;
	}
}