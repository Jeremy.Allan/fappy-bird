/// @description 
// You can write your code in this editor

shake = point_distance(mx_last, my_last, mouse_x, mouse_y);
//shake /= room_width;

mx_last = mouse_x;
my_last = mouse_y;


var gap_min = sprite_get_height(spr_wall) * 4;
var gap_max = sprite_get_height(spr_wall) * 6;

if (room = room0)
{
	if (spawn_tmr <= 0) 
	{
		wall_h_btm = random_range(0.1, 5);
		wall_h_top = random_range(0.1, 5);
	
		if ((sprite_get_height(spr_wall) * wall_h_top) > (room_height-wall_h_btm + gap_min))
		{
			while ((sprite_get_height(spr_wall) * wall_h_top) > (room_height-wall_h_btm + gap_min))
			{
				wall_h_top = random_range(0.1, 5);
			}
		}

		if ((sprite_get_height(spr_wall) * wall_h_top) > (room_height-wall_h_btm + gap_min))
		{
			while ((sprite_get_height(spr_wall) * wall_h_top) > (room_height-wall_h_btm + gap_min))
			{
				wall_h_top = random_range(0.1, 5);
			}
		}
	
		//while ((sprite_get_height(spr_wall) * wall_h_top) < (room_height-wall_h_btm + gap_max))
		//{
		//	wall_h_top = random_range(0.1, 5);
		//}
	
		with (instance_create_layer(room_width, room_height, "walls", obj_wall))
		{
			image_yscale = -other.wall_h_btm;
		}
	
		with (instance_create_layer(room_width, 0, "walls", obj_wall))
		{
			image_yscale = other.wall_h_top;
		}

	
		// setting the image y to a negative value also solves the issue of the object spawning at the bottom corner of the screen
		// this can also be solved by setting the origin of the sprite to the bottom instead
		spawn_tmr = spawn_tmr_max;
	}
	else
	{
		spawn_tmr--;
	}
}
