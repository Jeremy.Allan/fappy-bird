{
    "id": "d9d790f7-b7a1-486a-8335-88d9f8989932",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "controller",
    "eventList": [
        {
            "id": "cfd64422-e9bf-4562-be83-5d71b6708f62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d9d790f7-b7a1-486a-8335-88d9f8989932"
        },
        {
            "id": "e70c2be1-422d-407d-89ef-ba5c1eabc7b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9d790f7-b7a1-486a-8335-88d9f8989932"
        },
        {
            "id": "86327312-99f1-400a-bdb9-31acafc283c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9d790f7-b7a1-486a-8335-88d9f8989932"
        },
        {
            "id": "880e9411-a14b-4a2c-a322-5c9c0174dca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "d9d790f7-b7a1-486a-8335-88d9f8989932"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}