{
    "id": "2c062486-736e-4faa-985b-c4047ea0e560",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player",
    "eventList": [
        {
            "id": "c0441933-37b6-49a0-b5a8-f08f8c8f915f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c062486-736e-4faa-985b-c4047ea0e560"
        },
        {
            "id": "6f4c15b0-1941-4cb2-a76f-3bcdd7909852",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c062486-736e-4faa-985b-c4047ea0e560"
        },
        {
            "id": "0e170a9c-e8f8-4b1c-b95e-b700732f72d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "e53da1e1-28f0-4e9b-982f-02936b6d20ff",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2c062486-736e-4faa-985b-c4047ea0e560"
        },
        {
            "id": "83676233-ff67-42bb-b093-d2d417ceb71e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2c062486-736e-4faa-985b-c4047ea0e560"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "03ab947f-62f2-45c4-8e15-fa1fe21cc5bb",
    "visible": true
}